import React from 'react';
import { Link } from 'react-router-dom';

const Navbar =()=>{
    
    return(
        <div>
            <ul>
                <Link to="/solutions"><li>Solutions</li></Link>
                <Link to="/products"><li>Products</li></Link>
                <Link to="/partners"><li>Partners</li></Link>
                <Link to="/resources"><li>Resources</li></Link>
                <Link to="/about"><li>About</li></Link>
                
                
                
                
            </ul>
            
        </div>
    )
}
export default Navbar;

