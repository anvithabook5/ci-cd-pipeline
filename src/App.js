import React from 'react';
import Navbar  from './Navbar';
import {BrowserRouter,Switch,Route}from 'react-router-dom';
import { Card ,Button} from 'react-bootstrap';
import Solutions from './Solutions';
import Products from './Products';
import Partners from './Partners';
import Resources from './Resources';
import About from './About';
import "./App.css";

import Login from './Login';



  const App=()=>{

  return(
    <div>
    
        <BrowserRouter>
            
            
          <Navbar/>
            <Switch>
          
        
            <Route path="/solutions" exact component={Solutions}/>
            <Route path="/products" exact component={Products} />
            <Route path="/partners" exact component={Partners}/>
            <Route path="/resources" exact component={Resources}/>
            <Route path="/about" exact component={About}/>
            
          </Switch>
      </BrowserRouter>
      
      
      <div class="content">
      <div class="main-container">
        <div class="display-large">
          <div id="image-75331" class="bg-image" src="	https://www.cloudhealthtech.com/sites/default/file…es/Background/banner-ch-for-aws.jpg?itok=UvJxsFoZ"></div>
        </div>
      </div>
      
      <div class="container">
        <div class="main-container">
          <div class="large-container">
            <div id="bg-image-75331" class="bg-image"></div>
           
           
                  
            <div className='mb-3'>
            <Card style={{ backgroundColor:"#282561" ,width: '350%',height:'25rem'}}>
            <Card.Body>
            <div class="text-heading">Products</div>
              <div class="text-title">CloudHealth for Aws</div>
              <div class="text-content">Simplify financial management and find ways to lower your AWS bill, <br/>
              streamline workflows with custom policies that automate daily cloud <br/>
              operations, and strengthen your AWS security posture.</div>
              <div class="text-media"></div>
              <div class="button">
              <Button className="mb-3" variant="danger"style={{backgroundColor:"white",color:"#A52A2A"}}>Book Demo </Button></div>
              </Card.Body>
              
            </Card>
            </div>
            </div>
          </div>
      </div>
      <div class="three-up-container">
        <div class="three-up-container-header">
          <h4 class="headline">Manage Your AWS Environment at Scale</h4>
        </div>
        <div class="three-up-container-highlight-items">
          <div class="hightlight_items--item_1"></div>
            
            
          <div class="hightlight_items--item_2">
            
            
          </div>
          
          
            <div class="field field-name-field-icon field-type-clarity-iconfield-clarity-icon field-label-hidden field_item">Gain Visibility 
              <div class="clarity-icon"></div>
              <div class="field field-name-field-content field-type-clarity-iconfield-clarity-icon field-label-hidden field_item">Streamline decision-making <br/>
              with custom reports and <br/> 
              dashboards tailored to your <br/>
              business function, team, or <br/>
              project</div>
            </div>
            <div class="field field-name-field-headline field-type-clarity-iconfield-clarity-icon field-label-hidden field_item">OPTIMIZE RESOURCES
            <div class="field field-name-field-content field-type-clarity-iconfield-clarity-icon field-label-hidden field_item">View detailed breakdowns <br/>
             for major spending areas to <br/>
             track trends, watch for <br/>
             anomalies, and monitor <br/>
             usage and performance data</div>
            </div>
            <div class="field field-name-field-headline field-type-string field-label-hidden field_item">GOVERN USAGE          
            
              
            <div class="field field-name-field-content field-type-string-long field-label-hidden field_item">Build policies to automate <br/>
            daily operational tasks, <br/>
            improve security, and <br/>
            maintain tagging hygiene as <br/>
            you scale
            </div>
            </div>
           
            <div class="field field-name-field-headline field-type-string field-label-hidden field_item">STRENGTH SECURITY          
            
              
            <div class="field field-name-field-content field-type-string-long field-label-hidden field_item">Proactively manage security <br/>
            risk, alert users to critical <br/>
            issues ranked by severity, <br/>
            and gain deep insight into <br/>
            affected resources <br/>
          </div>
          </div>
      </div>
    </div>
    <div class="pargragh paragragh-text-media paragragh-view-mode-default text-media image-right ">
      <div class="text-media-container">
        <div class="text-media-container-text">
          
          <h3 class="field-name-headline">Gain Visibility Into Your AWS <br/>
          Environment</h3>
          <div class="block">
          <div class="field-name-field-text">
          
          <span>Empower stakeholders to analyze and report on your AWS <br/>
            environment in a way that's most impactful to them
          </span>
          </div>
          <div class="items-content">
          <ul>
          <ul>
            <li>
              <span>Dynamically allocate assets and costs to custom business <br/>
                groupings &nbsp;
              </span>
            </li>
          </ul>
            <ul>
            <li>
              <span>View cost, usage, and performance metrics holistically or segment <br/>
                by specific business grouping for granular analysis &nbsp;
              </span>
            </li>
          </ul>
          <ul>
            <li>
              <span>  Quickly and easily share valuable insights with individuals across <br/>
                your organization by sharing or subscribing them to CloudHealth <br/>
                reports  &nbsp;
              </span>
            </li>
          </ul>
          <ul>
            <li>
              <span>  
              Gain granular visibility into your Amazon ECS and EKS environments <br/>
              to analyze resource utilization metrics and optimize your container <br/>
              environment
                  &nbsp;
              </span>
            </li>
          </ul>
          </ul>
        </div>
        </div>
      </div>
      </div>
    </div>
      
      
      
  </div>
  </div>
    
    
    

  
    
  )
}
export default App;